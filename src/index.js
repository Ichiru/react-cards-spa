import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './Components/App/';

import { Provider } from 'react-redux';
import store from 'redux/store/store.js';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
