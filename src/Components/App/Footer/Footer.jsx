import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import style from './footer.module.scss';

const sn = classNames(style);

export const Footer = () => {
  return <div className={sn('footer')}></div>;
};
