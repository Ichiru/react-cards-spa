import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import style from './header.module.scss';

const sn = classNames(style);

export const Header = props => {
  return <div className={sn('header')}>{props.children}</div>;
};
