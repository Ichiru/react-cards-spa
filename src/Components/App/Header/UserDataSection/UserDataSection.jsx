import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import style from './userDataSection.module.scss';

const sn = classNames(style);

export const NameSection = props => {
  return <div className={sn('data-section')}>{props.children}</div>;
};
