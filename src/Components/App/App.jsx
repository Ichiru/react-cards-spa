import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './app.module.scss';

import { Header, UserNameSection } from './Header/';
import Main from './Main/';
import Footer from './Footer/';

import { BrowserRouter as Router } from 'react-router-dom';
import { useSelector } from 'react-redux';

const sn = classNames(styles);

export const App = () => {
  const { firstName, lastName } = useSelector(
    state => state.setUserReducer.user
  );
  return (
    <div className={sn('app')}>
      <Header>
        <UserNameSection>{firstName}</UserNameSection>
        <UserNameSection>{lastName}</UserNameSection>
      </Header>
      <Router>
        <Main />
      </Router>
      <Footer />
    </div>
  );
};
