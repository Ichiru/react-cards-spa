import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './userView.module.scss';

import { useSelector } from 'react-redux';

const sn = classNames(styles);

export const UserView = () => {
  const {
    avatar: { image, alt },
    status
  } = useSelector(state => state.setUserReducer);
  return (
    <div>
      <img className={sn('main__user-image')} src={image} alt={alt} />
      <div className={sn('user-view__user-status-slock')}>{status.text}</div>
    </div>
  );
};
