import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './loginPage.module.scss';
import Input from './Input';
import { mockedUserInfoCall } from 'api/userInfo';
import { signIn } from 'redux/actions/loginPageActions';
import { connect } from 'react-redux';

const sn = classNames(styles);

class LoginPage extends React.Component {
  state = {
    values: {}
  };

  onInputRegister = name => {
    if (this.state.values[name] === undefined) {
      this.setState(({ values, areValid }) => ({
        values: {
          ...values,
          [name]: ''
        },
        areValid: {
          ...areValid,
          [name]: false
        }
      }));
    }
  };

  onInputChange = (name, value) => {
    this.setState(({ values }) => ({
      values: {
        ...values,
        [name]: value
      }
    }));
  };

  handleFormValidCheck = ({ values } = this.state) => {
    mockedUserInfoCall().then(({ login, password }) => {
      if (values['login'] !== login) {
        return;
      }
      if (values['password'] !== password) {
        return;
      }
      this.props.signIn();
    });
  };

  handleFormSubmit = e => {
    this.handleFormValidCheck();
  };

  render() {
    const { login, password } = this.state.values;

    return (
      <div className={sn('login-page')}>
        <Input
          name='login'
          capture='Login'
          type='text'
          value={login}
          onInputRegister={this.onInputRegister}
          onInputChange={this.onInputChange}
        />
        <Input
          name='password'
          capture='Password'
          type='password'
          value={password}
          onInputRegister={this.onInputRegister}
          onInputChange={this.onInputChange}
        />
        <button
          className={sn('login-page__submit-button')}
          onClick={this.handleFormSubmit}
        >
          SUBMIT
        </button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.loginReducer
});

export const ConLoginPage = connect(mapStateToProps, { signIn })(LoginPage);
