import React, { useState, useEffect } from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './input.module.scss';

const sn = classNames(styles);
export const Input = ({
  name,
  capture,
  type,
  value,
  onInputRegister,
  onInputChange,
}) => {
  const [currentValue] = useState(value);

  useEffect(() => {
    onInputRegister(name);
  });

  const handleInputChange = e => {
    onInputChange(name, e.target.value);
  };

  return (
    <div>
      <label className={sn('input__input-label')} htmlFor={name}>
        {capture}:
        <input
          name={name}
          type={type}
          id={name}
          value={currentValue}
          onChange={handleInputChange}
          className={sn('input__input-field')}
        />
      </label>
    </div>
  );
};

//export const SuperInput = withValidationInputHOC(Input);
