import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './navigation.module.scss';

import { Link } from 'react-router-dom';
import { signOut } from 'redux/actions/loginPageActions';
import { useDispatch } from 'react-redux';

const sn = classNames(styles);

export const Navigation = () => {
  const dispatch = useDispatch();

  const handleLogOut = () => {
    dispatch(signOut());
  };

  return (
    <div className={sn('navigation')}>
      <Link to='/protectedCards' className={sn('navigation__wrapped-link')}>
        <button className={sn('navigation__button')}>Cards</button>
      </Link>
      <Link to='/protectedProfile'>
        <button className={sn('navigation__button')}>Profile</button>
      </Link>
      <Link to='/login'>
        <button className={sn('navigation__button')} onClick={handleLogOut}>
          Log out
        </button>
      </Link>
    </div>
  );
};
