import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './cardsContainer.module.scss';

import Card from './Card/';
import Loader from './Loader/';
import CardsCreationForm from './CardsCreationForm/';
import ShowFormButton from './ShowFormButton/';

import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { handleCardsRequest } from 'redux/thunks/handleCardsRequest';

const sn = classNames(styles);

class CardsContainer extends React.Component {
  componentDidMount() {
    this.props.handleCardsRequest();
  }

  render() {
    const { cards, isLoading, isCardsCreationFormVisible } = this.props;

    if (isLoading) {
      return (
        <div className={sn('cards-container')}>
          <Loader />
        </div>
      );
    }

    return (
      <div className={sn('cards-container')}>
        <ShowFormButton />

        {cards.length > 0 ? (
          cards.map(cardProps => <Card key={cardProps.id} card={cardProps} />)
        ) : (
          <div className={sn('cards-container__no-cards-class')}>
            No cards yet
          </div>
        )}

        {isCardsCreationFormVisible && <CardsCreationForm />}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  cards: state.cardsReducer,
  isLoading: state.loadingSpinnerReducer,
  isCardsCreationFormVisible: state.isVisibleCardCreationFormReducer
});

export const ConCardsContainer = withRouter(
  connect(mapStateToProps, {
    handleCardsRequest
  })(CardsContainer)
);
