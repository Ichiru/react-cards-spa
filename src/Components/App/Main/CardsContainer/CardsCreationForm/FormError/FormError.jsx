import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './formError.module.scss';

const sn = classNames(styles);

export const FormError = ({ error }) => {
  return (
    <div className={sn('formError')}>
      {error ? (
        <div className={sn('formError__error-container')}>{error}</div>
      ) : null}
    </div>
  );
};
