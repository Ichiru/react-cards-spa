import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './cardsCreationForm.module.scss';

import Input from './Input';
import RadioInput from './RadioInput';

import { connect } from 'react-redux';
import {
  addCard,
  showForm,
  hideForm
} from 'redux/actions/cardsCreationFormActions';

import img9 from 'assets/Card/img/cardImg9.jpg';

const sn = classNames(styles);

class CardsCreationForm extends React.Component {
  state = {
    values: { image: img9 },
    areValid: {}
  };

  handleAddCard = ({ tittle, price, image, gender }) => {
    const newCard = { tittle, price, image, gender };
    const { cards } = this.props;

    newCard.id =
      cards[cards.length - 1] !== undefined
        ? cards[cards.length - 1].id + 1
        : 1;

    this.props.addCard(newCard);
  };

  onInputRegister = name => {
    if (this.state.values[name] === undefined) {
      this.setState(({ values, areValid }) => ({
        values: {
          ...values,
          [name]: ''
        },
        areValid: {
          ...areValid,
          [name]: false
        }
      }));
    }
  };

  onInputChange = (name, value) => {
    this.setState(({ values }) => ({
      values: {
        ...values,
        [name]: value
      }
    }));
  };

  onInputSetValidStatus = (name, status) => {
    this.setState(({ areValid }) => ({
      areValid: {
        ...areValid,
        [name]: status
      }
    }));
  };

  handleFormValidCheck = ({ areValid } = this.state) => {
    for (let status in areValid) {
      if (areValid[status] === false) {
        return false;
      }
    }
    return true;
  };

  handleFormSubmit = e => {
    e.preventDefault();
    const isFormValid = this.handleFormValidCheck();

    if (!isFormValid) {
      return;
    }

    this.props.hideForm();
    this.handleAddCard(this.state.values);
  };

  handleFormClose = () => {
    this.props.hideForm();
  };

  render() {
    const { tittle, price, image, gender } = this.state.values;

    return (
      <div className={sn('form-container')}>
        <form className={sn('form-container__form')}>
          <header className={sn('form__header')}>
            <span>Add Card Form</span>
            <button
              className={sn('form__close-button')}
              onClick={this.handleFormClose}
            ></button>
          </header>
          <section className={sn('form__section')}>
            <Input
              name='tittle'
              capture='Tittle'
              type='text'
              value={tittle}
              onInputRegister={this.onInputRegister}
              onInputChange={this.onInputChange}
              validate={v => {
                if (v === '') {
                  return 'The field is required!';
                }
                if (v.length < 3) {
                  return 'Too short name!';
                }
                return '';
              }}
              onInputSetValidStatus={this.onInputSetValidStatus}
            />
            <Input
              name='price'
              capture='Price'
              type='number'
              value={price}
              onInputRegister={this.onInputRegister}
              onInputChange={this.onInputChange}
              validate={v => {
                if (v < 0) {
                  return 'Should be positive';
                }
                if (v === '') {
                  return 'The field is required!';
                }
                return '';
              }}
              onInputSetValidStatus={this.onInputSetValidStatus}
            />
            <Input
              name='image'
              capture='Image'
              type='text'
              value={image}
              onInputRegister={this.onInputRegister}
              onInputChange={this.onInputChange}
              validate={v => {
                if (v === '') {
                  return 'The field is required!';
                }
                return '';
              }}
              onInputSetValidStatus={this.onInputSetValidStatus}
            />
            <RadioInput
              name='gender'
              capture='Gender'
              type='radio'
              selects={['men', 'women', 'any']}
              value={gender}
              onInputRegister={this.onInputRegister}
              onInputChange={this.onInputChange}
              validate={v => {
                if (v === '') {
                  return 'The field is required!';
                }
                return '';
              }}
              onInputSetValidStatus={this.onInputSetValidStatus}
            />
            <button
              className={sn('section__submit-button')}
              onClick={this.handleFormSubmit}
            >
              SUBMIT
            </button>
          </section>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  cards: state.cardsReducer
});

export const ConCardsCreationForm = connect(mapStateToProps, {
  addCard,
  showForm,
  hideForm
})(CardsCreationForm);
