import React, { useState, useEffect } from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './radioInput.module.scss';
import FormError from '../FormError';

const sn = classNames(styles);

export const RadioInput = ({
  name,
  capture,
  selects,
  value,
  onInputRegister,
  onInputChange,
  validate,
  onInputSetValidStatus,
}) => {
  const [currentValue] = useState(value);
  const [currentError, changeError] = useState('');

  useEffect(() => {
    onInputRegister(name);
  });

  const handleInputChange = e => {
    onInputChange(name, e.target.id);
  };

  const handleValidate = e => {
    const error = validate(e.target.id);

    if (error) {
      onInputSetValidStatus(name, false);
    } else {
      onInputSetValidStatus(name, true);
    }

    changeError(error);
  };

  return (
    <div className={sn('radio-input')}>
      <FormError error={currentError} />
      <span className={sn('radio-input__input-label')}>{capture}:</span>
      <div className={sn('radio-input__inputs-area')}>
        {selects.map(id => (
          <label
            className={sn('inputs-area__input-label')}
            htmlFor={id}
            key={id}
          >
            {id}:
            <input
              name={name}
              type='radio'
              id={id}
              value={currentValue}
              onChange={handleInputChange}
              onBlur={handleValidate}
              className={sn('inputs-area__input-field')}
            />
          </label>
        ))}
      </div>
    </div>
  );
};
