import React, { useState, useEffect } from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './input.module.scss';
import FormError from '../FormError';

const sn = classNames(styles);

export const Input = ({
  name,
  capture,
  type,
  value,
  onInputRegister,
  onInputChange,
  validate,
  onInputSetValidStatus
}) => {
  const [currentValue] = useState(value);
  const [currentError, changeError] = useState('');

  useEffect(() => {
    onInputRegister(name);
  });

  const handleInputChange = e => {
    onInputChange(name, e.target.value);
  };

  const handleValidate = e => {
    const error = validate(e.target.value);

    if (error) {
      onInputSetValidStatus(name, false);
    } else {
      onInputSetValidStatus(name, true);
    }

    changeError(error);
  };

  return (
    <div>
      <FormError error={currentError} />
      <label className={sn('input__input-label')} htmlFor={name}>
        {capture}:
        <input
          name={name}
          type={type}
          id={name}
          value={currentValue}
          onChange={handleInputChange}
          onBlur={handleValidate}
          className={sn('input__input-field')}
        />
      </label>
    </div>
  );
};
