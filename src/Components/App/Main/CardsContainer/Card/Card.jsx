import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import { passProp } from 'utilities/passPropToObject';
import styles from './card.module.scss';

import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { deleteCard } from 'redux/actions/cardActions';

const sn = classNames(styles);

class Card extends React.Component {
  handleDelete = () => {
    this.props.deleteCard(this.props.card.id);
  };

  render() {
    const {
      card: { tittle, price, image, gender }
    } = this.props;

    const tittleColors = {
      men: 'item__tittle_blue',
      women: 'item__tittle_pink'
    };
    const genderTittleColor = passProp(
      tittleColors,
      gender,
      'item__tittle_orange'
    );

    return (
      <div className={sn('item')}>
        <img
          className={sn('item__picture')}
          src={image}
          alt={`${tittle}  -  ${gender}`}
        />
        <div className={sn('item__cover')}>
          <div className={sn('item__tittle')}>
            <span>{tittle}</span>
            <br />
            <span className={sn(genderTittleColor)}>{gender}</span>
            <br />
            <span className={sn('item__price')}>{price}</span>
          </div>
          <button
            className={sn('item__delete-button')}
            onClick={this.handleDelete}
          ></button>
        </div>
      </div>
    );
  }
}

export const ConCard = withRouter(
  connect(null, {
    deleteCard
  })(Card)
);
