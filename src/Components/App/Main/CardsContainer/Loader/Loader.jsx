import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './Loader.module.scss';

const sn = classNames(styles);

export const Loader = () => {
  return (
    <div className={sn('cssload-container')}>
      <div className={sn('cssload-whirlpool')}></div>
    </div>
  );
};
