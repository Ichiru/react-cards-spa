import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './showFormButton.module.scss';
import { connect } from 'react-redux';
import { showForm } from 'redux/actions/cardsCreationFormActions';

const sn = classNames(styles);

export class ShowFormButton extends React.Component {
  handleOpenForm = () => {
    this.props.showForm();
  };

  render() {
    return (
      <div className={sn('show-form-button-wrapper')}>
        <button
          className={sn('show-form-button-wrapper__button')}
          onClick={this.handleOpenForm}
        >
          Add new card
        </button>
      </div>
    );
  }
}

export const ConShowFormButton = connect(null, {
  showForm
})(ShowFormButton);
