import React from 'react';
import { classNames } from 'utilities/classNameUtility';
import styles from './main.module.scss';

import UserView from './UserView/';
import CardsContainer from './CardsContainer/';
import Navigation from './Navigation';
import LoginPage from './LoginPage';
import ProfilePage from './ProfilePage';

import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

const sn = classNames(styles);

class Main extends React.Component {
  render() {
    const { isAuthenticated } = this.props;
    return (
      <div className={sn('main')}>
        <UserView />
        <div className={sn('main__inner-container')}>
          <Navigation />

          {isAuthenticated ? (
            <Redirect to='/cards' from='/login' />
          ) : (
            <Redirect to='/login' from='*' />
          )}

          <Switch>
            <Route
              path='/protectedCards'
              render={() =>
                isAuthenticated ? (
                  <Redirect to='/cards' from='*' />
                ) : (
                  <Redirect to='/login' from='*' />
                )
              }
            />
            <Route
              path='/protectedProfile'
              render={() =>
                isAuthenticated ? (
                  <Redirect to='/profile' from='*' />
                ) : (
                  <Redirect to='/login' from='*' />
                )
              }
            />
            <Route exact path='/login' render={() => <LoginPage />} />
            <Route exact path='/cards' component={CardsContainer} />
            <Route exact path='/profile' component={ProfilePage} />
          </Switch>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.logInReducer
});

export const conMain = withRouter(connect(mapStateToProps, null)(Main));
