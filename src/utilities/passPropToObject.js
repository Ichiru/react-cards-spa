import get from 'lodash';

export const passProp = (object, property, defaultProperty) => {
  return get.get(object, property, defaultProperty);
};
