import classnames from 'classnames/bind';

export const classNames = styles => classnames.bind(styles);
