export const receiveCards = cards => {
  return {
    type: 'RECEIVE_CARDS',
    payload: cards
  };
};

export const showLoadingSpinner = () => {
  return {
    type: 'SHOW_SPINNER'
  };
};

export const hideLoadingSpinner = () => {
  return {
    type: 'HIDE_SPINNER'
  };
};
