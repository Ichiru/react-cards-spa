export const deleteCard = deletedId => {
  return {
    type: 'DELETE_CARD',
    payload: deletedId
  };
};
