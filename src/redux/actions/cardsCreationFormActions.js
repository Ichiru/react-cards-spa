export const addCard = card => {
  return {
    type: 'ADD_CARD',
    payload: card
  };
};

export const showForm = () => {
  return {
    type: 'SHOW_FORM'
  };
};

export const hideForm = () => {
  return {
    type: 'HIDE_FORM'
  };
};
