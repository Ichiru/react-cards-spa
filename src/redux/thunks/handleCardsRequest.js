import { mockedApiCall } from 'api/mockedApi';
import {
  receiveCards,
  showLoadingSpinner,
  hideLoadingSpinner
} from 'redux/actions/cardsContainerActions';

export const handleCardsRequest = () => dispatch => {
  dispatch(showLoadingSpinner());
  mockedApiCall()
    .then(cards => dispatch(receiveCards(cards)))
    .then(() => dispatch(hideLoadingSpinner()));
};
