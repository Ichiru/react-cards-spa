import img from 'assets/Main/img/chef-photo.png';

const initialState = {
  user: {
    firstName: 'Andrew',
    lastName: 'Hopkins'
  },
  avatar: {
    image: img,
    alt: 'image'
  },
  status: {
    text: "Hi, I'm a seller of this cool sportswear..."
  }
};

export const setUserReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
