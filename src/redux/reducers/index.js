import { combineReducers } from 'redux';

import { cardsReducer } from './cardsReducer';
import { loadingSpinnerReducer } from './loadingSpinnerReducer';
import { logInReducer } from './logInReducer';
import { isVisibleCardCreationFormReducer } from './isVisibleCardCreationFormReducer';
import { setUserReducer } from './setUserReducer';

export default combineReducers({
  cardsReducer,
  loadingSpinnerReducer,
  logInReducer,
  isVisibleCardCreationFormReducer,
  setUserReducer
});
