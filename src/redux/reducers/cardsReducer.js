export const cardsReducer = (state = [], action) => {
  switch (action.type) {
    case 'RECEIVE_CARDS':
      return action.payload;
    case 'ADD_CARD':
      return [...state, action.payload];
    case 'DELETE_CARD':
      return state.filter(({ id }) => id !== action.payload);
    default:
      return state;
  }
};
