import { mockedResponse } from 'api/mockedResponse';

export const mockedApiCall = () => {
  return new Promise(resolve => {
    setTimeout(() => resolve(mockedResponse), 500);
  });
};
