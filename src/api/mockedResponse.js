import img1 from "assets/Card/img/cardImg1.jpg";
import img2 from "assets/Card/img/cardImg2.jpg";
import img3 from "assets/Card/img/cardImg3.jpg";
import img4 from "assets/Card/img/cardImg4.jpg";
import img5 from "assets/Card/img/cardImg5.jpg";
import img6 from "assets/Card/img/cardImg6.jpg";
import img7 from "assets/Card/img/cardImg7.jpg";
import img8 from "assets/Card/img/cardImg8.jpg";

export const mockedResponse = [
  {
    id: 1,
    tittle: "Sport jacket",
    price: 1199,
    image: img1,
    gender: "men"
  },
  {
    id: 2,
    tittle: "Sweatpants",
    price: 40,
    image: img2,
    gender: "men"
  },
  {
    id: 3,
    tittle: "Sport t-shirt",
    price: 38,
    image: img3,
    gender: "men"
  },
  {
    id: 4,
    tittle: "Sport shorts",
    price: 32,
    image: img4,
    gender: "men"
  },
  {
    id: 5,
    tittle: "Men's Snickers",
    price: 430,
    image: img5,
    gender: "men"
  },
  {
    id: 6,
    tittle: "Wristband",
    price: 12,
    image: img6,
    gender: "unisex"
  },
  {
    id: 7,
    tittle: "leggings",
    price: 78,
    image: img7,
    gender: "women"
  },
  {
    id: 8,
    tittle: "Women sportswear suit",
    price: 235,
    image: img8,
    gender: "women"
  }
];
